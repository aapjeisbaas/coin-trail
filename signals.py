#!/usr/bin/env python
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
import time
from datetime import datetime, timedelta
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import signal
import json

# argparse
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--coin', action="store", default="ZEN", dest="coin")
parser.add_argument('--graph', action="store_true", default=False)
parser.add_argument('--verbose', action="store_true", default=False)
parser.add_argument('--json', action="store_true", default=False)
parser.add_argument('--invest-percent', action="store", default=5, dest="investPercent", type=float)
parser.add_argument('--invest-btc', action="store", default=0, dest="investBtc", type=float)
parser.add_argument('--live', action="store_true", default=False)
parser.add_argument('--slow-replay', action="store_true", dest="slowReplay", default=False)
parser.add_argument('--hist-days', action="store", default=1, dest="histDays", type=float)



args = parser.parse_args()

pair = str.upper(args.coin)+"/BTC"
if args.json:
    output = {}
    output['PAIR'] = pair
    output['ACTION'] = "none"
    output['PRICE'] = "none"
    output['SIZE'] = "none"
    print(json.dumps(output))
else:
    print(pair)

# Import the backtrader platform
import backtrader as bt

# dirty global sl thing 
sl = 0.0000000
slSize = 0.0000000

# start time for live trading
startTime = datetime.utcnow() - timedelta(minutes=15)

# Create a Stratey
class firstStrategy(bt.Strategy):

    def __init__(self):
        self.rsi = bt.indicators.RSI_SMA(self.data.close, period=21)
        signal.signal(signal.SIGINT, self.sigstop)

    def sigstop(self, a ,b):
        print('Stopping Backtrader')
        self.env.runstop()


    def next(self):
        global sl
        global slSize

        if not self.position:
            if self.rsi < 30:
                price = self.data.open[0]
                cash = self.broker.getcash()
                if args.investBtc == 0:
                    buySize = cash*(args.investPercent/100)
                    size = buySize/price
                else:
                    buySize = args.investBtc
                    size = buySize/price
                if args.verbose:
                    print("Price "+str(price)+" Cash "+str(cash)+" buySize "+str(buySize)+" size "+str(size))
                minTrade = 0.0005
                if buySize >= minTrade:
                    if args.verbose:
                        print("BUY INIT size: "+str(size)+" price: "+str(price)+" btc amount: "+str(size*price))
                    if args.json:
                        signal = {}
                        signal['PAIR'] = pair
                        signal['ACTION'] = 'BUY'
                        signal['PRICE'] = round(price, 8)
                        signal['SIZE'] = round(size, 8)
                        if args.live:
                            if self.data.datetime.datetime() > startTime:
                                print(json.dumps(signal))
                        else:
                            print(json.dumps(signal))
                    slSize = size 
                    self.buy(size=size, price=price)
                else:
                    if args.verbose:
                        print("trade too small")

        else:
            if self.data.open[0] < self.position.price*0.90:
                price = self.data.open[0]
                size = self.position.size*1.25
                if args.verbose:
                    print("BUY DCA size: "+str(size)+" price: "+str(price)+" amount: "+str(size*price))
                if args.json:
                        signal = {}
                        signal['PAIR'] = pair
                        signal['ACTION'] = 'BUY'
                        signal['PRICE'] = round(price, 8)
                        signal['SIZE'] = round(size, 8)
                        if args.live:
                            if self.data.datetime.datetime() > startTime:
                                print(json.dumps(signal))
                        else:
                            print(json.dumps(signal))
                slSize = slSize+size
                self.buy(size=size, price=price)

            if self.data.open[0] < sl:
                size = self.position.size
                if args.verbose:
                    print("Sell size: "+str(size)+" price: "+str(sl)+" amount: "+str(size*sl))
                self.sell(size=size, price=sl)
                sl = 0.0
                slSize = 0.0

            if slSize > 0:
                if self.data.open[0] > self.position.price*1.04:
                    current = self.data.open[0]
                    new_sl = current*0.96
                    if new_sl >= sl:
                        sl = new_sl
                        if args.verbose:
                            print("Time: "+str(self.data.datetime.datetime())+"Current Price: "+str(current)+" changing stop loss to: "+str(sl))
                        if args.json:
                            signal = {}
                            signal['PAIR'] = pair
                            signal['ACTION'] = 'SL'
                            signal['PRICE'] = round(sl, 8)
                            signal['SIZE'] = round(slSize, 8)
                            if args.live:
                                if self.data.datetime.datetime() > startTime:
                                    print(json.dumps(signal))
                            else:
                                print(json.dumps(signal))

        if args.slowReplay:
            time.sleep(0.1)     


    def notify_trade(self, trade):
        dt = self.data.datetime.datetime()
        cash = self.broker.getcash()
        if trade.isclosed:
            if args.verbose:
                print('{} {} Closed: PnL Gross {}, Net {}, Cash {}'.format(
                    dt,
                    trade.data._name,
                    round(trade.pnl, 8),
                    round(trade.pnlcomm, 8),
                    round(cash, 8)))


if __name__ == '__main__':
    # Create a cerebro entity
    cerebro = bt.Cerebro()


    #Add some strategy
    cerebro.addstrategy(firstStrategy)

    histMinutes = args.histDays*24*60 

    hist_start_date = datetime.utcnow() - timedelta(minutes=histMinutes)
    hist_stop_date = datetime.utcnow() - timedelta(minutes=15)

    if args.live:
        data_ticks = bt.feeds.CCXT(exchange='bittrex', symbol=pair, name="coin_btc_tick",
                           timeframe=bt.TimeFrame.Minutes, fromdate=hist_start_date, compression=5)
    else:
        data_ticks = bt.feeds.CCXT(exchange='bittrex', symbol=pair, name="coin_btc_tick",
                           timeframe=bt.TimeFrame.Minutes, fromdate=hist_start_date, todate=hist_stop_date, compression=5)

    # Add the Data Feed to Cerebro
    cerebro.adddata(data_ticks)

    # Set our desired cash start
    cerebro.broker.setcash(0.05)

    # Set the commission
    cerebro.broker.setcommission(commission=0.0025)

    # Print out the starting conditions
    if not args.json:
        print('Starting Portfolio Value: %.8f' % cerebro.broker.getvalue())

    # Run over everything
    cerebro.run()

    # Print out the final result
    if not args.json:
        print('Final Portfolio Value: %.8f' % cerebro.broker.getvalue())

    # Plot the result
    if args.graph:
      cerebro.plot()
