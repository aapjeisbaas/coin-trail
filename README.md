![Sorry but I like to keep track of my projects](https://perzik.aapjeisbaas.nl/perzik.perzik?idsite=4&rec=1&action_name=coin-trail)
## Setup

For questions, suggestion and feature requests email the project at:
`incoming+aapjeisbaas/coin-trail@gitlab.com`

```
sudo pip install -r requirements.txt
```

Run and test it:
```
 $ ./signals.py --help
usage: signals.py [-h] [--coin COIN] [--graph] [--verbose] [--json]
                  [--invest-percent INVESTPERCENT] [--invest-btc INVESTBTC]
                  [--live] [--slow-replay] [--hist-days HISTDAYS]

optional arguments:
  -h, --help            show this help message and exit
  --coin COIN
  --graph
  --verbose
  --json
  --invest-percent INVESTPERCENT
  --invest-btc INVESTBTC
  --live
  --slow-replay
  --hist-days HISTDAYS

# Example:
./signals.py --coin zen --graph --verbose
```


