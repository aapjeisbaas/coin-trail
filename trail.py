#!/usr/bin/env python
from bittrex.bittrex import *
import yaml
import threading
import shlex
from datetime import datetime, timedelta
from queue import Queue
import time
import json
from subprocess import Popen, PIPE, STDOUT
import telegram
import argparse

from argparse import RawTextHelpFormatter

parser = argparse.ArgumentParser(description='trail them coins to the moon and beyond', formatter_class=RawTextHelpFormatter)
parser.add_argument('--trade-mode', action="store", default=1, dest="tradeMode", type=int,
                     help="  Mode options: \n"
                          "  1 live trading (default) \n"
                          "  2 sell only mode \n"
                          "  3 dry humping, no actions on exchange")

args = parser.parse_args()

trade_lock = threading.Lock()

with open("settings.yml", 'r') as cfgfile:
    try:
         cfg = yaml.load(cfgfile)
         ApiKey = cfg['credentials']['ApiKey']
         ApiSecret = cfg['credentials']['ApiSecret']
         TgBotToken = cfg['telegram']['BotToken']
         TgChatId = cfg['telegram']['ChatId']
    except yaml.YAMLError as exc:
         print(exc)

v2_bittrex = Bittrex(ApiKey, ApiSecret, api_version=API_V2_0)
v1_bittrex = Bittrex(ApiKey, ApiSecret, api_version=API_V1_1)
bot = telegram.Bot(token=TgBotToken)
balance = v1_bittrex.get_balances()

#bot.send_message(chat_id=TgChatId, text='<b>' + "start trail" + '</b>', parse_mode=telegram.ParseMode.HTML)

def process_signals(coin):
    broken = 0
    while broken < 15:
        signal_cmd = "python3 -u signals.py --json --live --coin "+str(coin)
        cmd = shlex.split(signal_cmd)
        startTime = datetime.utcnow()
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        for line in p.stdout:
            jsonLine = json.loads(line)
            action = jsonLine['ACTION']
            price = jsonLine['PRICE']
            size = jsonLine['SIZE']
            pair = str.upper("BTC-"+str(coin))
            orderHistory = v1_bittrex.get_order_history(market=pair)
            ordersOpen = v1_bittrex.get_open_orders(market=pair)
            print(orderHistory)
            print(ordersOpen)
            print(size)
            if action == "BUY":
                message = str("Buy: "+str(size)+" of "+str(coin)+" at a price of Ƀ"+str(price))
                with trade_lock:
                    if args.tradeMode == 1:
                        print(v1_bittrex.buy_limit(market=pair, quantity=size, rate=price))
                    print(str(jsonLine))
                    print(message)
                    bot.send_message(chat_id=TgChatId, text=message)
            if action == "SL":
                message = str("Increase "+str(coin)+"'s SL to: Ƀ"+str(price))
                with trade_lock:
                    if (args.tradeMode == 1) or (args.tradeMode == 2):
                        for order in ordersOpen['result']:
                            if order['Quantity'] == size:
                                print(v1_bittrex.cancel(uuid=str(order['OrderUuid'])))
                        print(v2_bittrex.trade_sell(market=pair, order_type="LIMIT", quantity=size, rate=price, time_in_effect="GOOD_TIL_CANCELLED", condition_type="LESS_THAN", target=price))
                    print("UTC: "+str(datetime.utcnow()))
                    print(str(jsonLine))
                    print(message)
                    bot.send_message(chat_id=TgChatId, text=message)
            #else:
            #    message = str("Trail enabled for: "+str(coin))
            #    with trade_lock:
            #        print(str(jsonLine))
            #        print(message)
            #        bot.send_message(chat_id=TgChatId, text=message)
        stderr = p.communicate()
        endTime = datetime.utcnow()
        if (endTime - startTime) > timedelta(minutes=30):
            broken = 0
        broken += 1
        print("UTC: "+str(datetime.utcnow()))
        print(str(coin)+" Broken count: "+str(broken))
        print(stderr)
        sleeptime = 60*broken
        time.sleep(sleeptime)

def threader():
    while True:
        coin = q.get()
        process_signals(coin)
        q.task_done()

q = Queue()

for x in range(100):
    t = threading.Thread(target = threader)
    t.daemon = True
    t.start()


coins = []

markets = v1_bittrex.get_market_summaries()
# top volume coins:
for market in markets['result']:
    if "BTC-" in market['MarketName'] and market['BaseVolume'] > 500 and market['Low'] > 0.0001 and market['OpenBuyOrders'] > 500 and "BCC" not in market['MarketName']:
        pair = market['MarketName']
        coin = pair.split("-")[1]
        coins.append(coin)

for coin in balance['result']:
    if coin['Balance'] > 0 and coin['Currency'] != "BTC" and coin['Currency'] != "BTS":
        coins.append(coin['Currency'])

# insert the list to the set
list_set = set(coins)
# convert the set to the list
unique_coins = (list(list_set))

for coin in unique_coins:
    q.put(coin)
    print(coin)
    # don't overload bittrex api
    time.sleep(23)



#for pair in cfg['pairs']:
#    Amount = cfg['pairs'][pair]['Amount']
#    MinSell = cfg['pairs'][pair]['MinSell']
#   Base = pair.split("-")[0]
#   Coin = pair.split("-")[1]
#   Balance = my_bittrex.get_balance(Coin)
    
#    print("Base: "+Base+" Coin: "+Coin+" Amount: "+str(Amount)+" Minimum sell price: "+str(MinSell))

q.join()
