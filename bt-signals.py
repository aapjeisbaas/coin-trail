# !/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-

from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
import time
from datetime import datetime, timedelta
import backtrader as bt

class TestStrategy(bt.Strategy):
    def next(self):
        print('*' * 5, 'NEXT:', bt.num2date(self.data.datetime[0]), self.data._name, self.data.open[0], self.data.high[0],
              self.data.low[0], self.data.close[0], self.data.volume[0],
              bt.TimeFrame.getname(self.data._timeframe), len(self.data))

if __name__ == '__main__':
    cerebro = bt.Cerebro()
    
    hist_start_date = datetime.utcnow() - timedelta(minutes=100)
    data_ticks = bt.feeds.CCXT(exchange='bittrex', symbol='ZEN/BTC', name="zen_btc_tick",
                           timeframe=bt.TimeFrame.Minutes, fromdate=hist_start_date, compression=1)

    cerebro.adddata(data_ticks)
    cerebro.addstrategy(TestStrategy)
    cerebro.run()
